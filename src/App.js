import React, { Component } from "react";
import { calculateResult } from "./utils";
import "./App.css";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: "",
      userInput: "",
      result: [],
      error: "Write something",
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    const { value } = event.target;
    this.setState({ value });
  }

  handleSubmit(event) {
    const { value } = this.state;
    const { input, result, error } = calculateResult(value);
    this.setState({ userInput: input, result, error });
    event.preventDefault();
  }
  render() {
    const { result, error } = this.state;
    return (
      <div className="App">
        <form className="App-form" onSubmit={this.handleSubmit}>
          <input
            type="text"
            value={this.state.value}
            onChange={this.handleChange}
            className="inputArray"
            placeholder="Input an Array ... "
          />
          {!error && (
            <p>
              <span>
                Result for input have {result.length} possible result.
              </span>
            </p>
          )}
          {error && <p className="App-error">{error}</p>}
        </form>
        {result.map((res) => {
          console.log(res);
          return (
            <div className="row w-100 d-flex justify-content-center mt-2">
              <div className="resultCard col-3">
                <span>pA : {res.pA}</span>
              </div>
              <div className="resultCard col-3">
                <span>pB : {res.pB}</span>
              </div>
              <div className="resultCard col-3">
                <span>sum : {res.sum}</span>
              </div>
            </div>
          );
        })}
      </div>
    );
  }
}

export default App;
