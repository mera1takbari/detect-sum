export const detectSums = (input) => {
  if (!input) throw Error("Input is not an array");

  let array;
  if (Array.isArray(input)) {
    array = input;
  } else if (typeof input === "string" && !input.includes("[")) {
    throw new Error("Input is not an array");
  } else if (typeof input === "string" && input.includes("[")) {
    array = Array.from(input)
      .filter((el) => el !== "," && el !== "[" && el !== "]")
      .map((el) => parseInt(el.trim(), 10));
  }

  const results = [];
  const hashTable = {};

  array.forEach((element, i) => {
    if (element in hashTable) hashTable[element].push(i);
    else hashTable[element] = [i];
  });

  for (let i = 0; i < array.length; i++) {
    for (let j = i + 1; j < array.length; j++) {
      const possibleSum = array[i] + array[j];
      if (hashTable[possibleSum]) {
        hashTable[possibleSum].forEach((element) => {
          if (i !== element && j !== element)
            results.push({
              pA: i,
              pB: j,
              sum: element,
            });
        });
      }
    }
  }

  return results;
};

export function calculateResult(input) {
  const parsedInput = input.split(",").map((i) => parseInt(i.trim(), 10));
  let error = null;
  let result = "";
  try {
    result = detectSums(input);
  } catch (e) {
    error = e.message;
  }
  return { input: parsedInput, result, error };
}
